[[_TOC_]]

## Aim of the program
This progral aims at computing molecular orbital decomposition from a Gaussian output file.

## System requirement
The program is written in python2. 

To use it, the minimal requirements are:
<ul>
	<li>Linux/MacOs operating system (will not work on windows)</li>
	<li>A python2.7 interpreter</li>
	<li>The numpy module</li>
</ul>

## Gaussian calculation requirement
Since all the decomposition methods needs OA overlaps and LCAO coeeficient, one should set the 2 following keywords in there input files:
<ul>
	<li><i>pop=full</i> in order to print the LCAO coefficient in the Gaussian output file</li>
	<li><i>IOP(3/33=1)</i> in order to print the Overlap matrix in the Gaussian output file</li>
</ul>
<b>Be carefull: Those 2 options creates extremely large outputs so only use them in SinglePoints calculations</b>

## How to use the program
<ol>
	<li>go to the directory where you have your output file</li>
	<li>star the python script</li>
	<li>answer the questions asked bu the script</li>
</ol>
