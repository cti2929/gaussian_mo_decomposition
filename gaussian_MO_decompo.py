import os
import numpy as np

print("OUTPUT FILE")
fichier=raw_input("")
f=os.popen("grep 'NAtoms=' "+fichier).readlines()
nat=int(str.split(f[0])[1])
f=os.popen("grep 'NBasis' "+fichier).readlines()
if str.split(f[0])[2].isdigit():
	nbasis=int(str.split(f[0])[2])
else:
	if str.split(f[0])[1][0]!='=':
		nbasis=int(str.split(f[0])[1])
	else:
		nbasis=int(str.split(f[0])[1][1:])
#print(nat,nbasis)
ngroup=int(nbasis/5)
reste=nbasis-ngroup*5
#if reste>0:
#	print(ngroup,reste)

def overlap(ngroup,reste,nbasis):
	f=os.popen('grep -A'+str(ngroup*(nbasis+1))+' " Overlap " '+fichier).readlines()
	print('ici')
	#print(str(ngroup*(nbasis+1)))
	S=np.zeros((nbasis,nbasis))
	ligne=1
	t=0
	t2=0
	t3=0
	if len(f)>0:
		t3=1
		while t==0:
			pos=int(str.split(f[ligne])[0])-1
			#print('POS ',pos,f[ligne])
			ligne=ligne+1
			
			S[pos][pos]=float(str.split(f[ligne])[1].replace("D","E"))
			ligne=ligne+1
			
			S[pos][pos+1]=float(str.split(f[ligne])[1].replace("D","E"))
			S[pos+1][pos]=S[pos][pos+1]
			S[pos+1][pos+1]=float(str.split(f[ligne])[2].replace("D","E"))
			ligne=ligne+1
			
			S[pos][pos+2]=float(str.split(f[ligne])[1].replace("D","E"))
			S[pos+2][pos]=S[pos][pos+2]
			S[pos+1][pos+2]=float(str.split(f[ligne])[2].replace("D","E"))
			S[pos+2][pos+1]=S[pos+1][pos+2]
			S[pos+2][pos+2]=float(str.split(f[ligne])[3].replace("D","E"))
			ligne=ligne+1
			
			S[pos][pos+3]=float(str.split(f[ligne])[1].replace("D","E"))
			S[pos+3][pos]=S[pos][pos+3]
			S[pos+1][pos+3]=float(str.split(f[ligne])[2].replace("D","E"))
			S[pos+3][pos+1]=S[pos+1][pos+3]
			S[pos+2][pos+3]=float(str.split(f[ligne])[3].replace("D","E"))
			S[pos+3][pos+2]=S[pos+2][pos+3]
			S[pos+3][pos+3]=float(str.split(f[ligne])[4].replace("D","E"))
			for i in range(pos+4,nbasis):
				ligne=ligne+1
				
				S[pos][i]=float(str.split(f[ligne])[1].replace("D","E"))
				S[i][pos]=S[pos][i]
				S[pos+1][i]=float(str.split(f[ligne])[2].replace("D","E"))
				S[i][pos+1]=S[pos+1][i]
				S[pos+2][i]=float(str.split(f[ligne])[3].replace("D","E"))
				S[i][pos+2]=S[pos+2][i]
				S[pos+3][i]=float(str.split(f[ligne])[4].replace("D","E"))
				S[i][pos+3]=S[pos+3][i]
				S[pos+4][i]=float(str.split(f[ligne])[5].replace("D","E"))
				S[i][pos+4]=S[pos+4][i]
			ligne=ligne+1
			t2=t2+1
			if t2>ngroup-1:t=1
		if reste>0:
			pos=int(str.split(f[ligne])[0])-1
			#print('POS ',pos,f[ligne])
        	        ligne=ligne+1
        	        if reste==1:
				S[pos][pos]=float(str.split(f[ligne])[1].replace("D","E"))
        	        elif reste==2:
				S[pos][pos]=float(str.split(f[ligne])[1].replace("D","E"))
				ligne=ligne+1
				S[pos][pos+1]=float(str.split(f[ligne])[1].replace("D","E"))
				S[pos+1][pos]=S[pos][pos+1]
				S[pos+1][pos+1]=float(str.split(f[ligne])[2].replace("D","E"))
			elif reste==3:
				S[pos][pos]=float(str.split(f[ligne])[1].replace("D","E"))
				ligne=ligne+1
				S[pos][pos+1]=float(str.split(f[ligne])[1].replace("D","E"))
				S[pos+1][pos]=S[pos][pos+1]
				S[pos+1][pos+1]=float(str.split(f[ligne])[2].replace("D","E"))
				ligne=ligne+1
				S[pos][pos+2]=float(str.split(f[ligne])[1].replace("D","E"))
				S[pos+2][pos]=S[pos][pos+2]
				S[pos+1][pos+2]=float(str.split(f[ligne])[2].replace("D","E"))
				S[pos+2][pos+1]=S[pos+1][pos+2]
				S[pos+2][pos+2]=float(str.split(f[ligne])[3].replace("D","E"))
			elif reste==4:
				S[pos][pos]=float(str.split(f[ligne])[1].replace("D","E"))
				ligne=ligne+1
				S[pos][pos+1]=float(str.split(f[ligne])[1].replace("D","E"))
				S[pos+1][pos]=S[pos][pos+1]
				S[pos+1][pos+1]=float(str.split(f[ligne])[2].replace("D","E"))
				ligne=ligne+1
				S[pos][pos+2]=float(str.split(f[ligne])[1].replace("D","E"))
				S[pos+2][pos]=S[pos][pos+2]
				S[pos+1][pos+2]=float(str.split(f[ligne])[2].replace("D","E"))
				S[pos+2][pos+1]=S[pos+1][pos+2]
				S[pos+2][pos+2]=float(str.split(f[ligne])[3].replace("D","E"))
				ligne=ligne+1
				S[pos][pos+3]=float(str.split(f[ligne])[1].replace("D","E"))
				S[pos+3][pos]=S[pos][pos+3]
				S[pos+1][pos+3]=float(str.split(f[ligne])[2].replace("D","E"))
				S[pos+3][pos+1]=S[pos+1][pos+3]
				S[pos+2][pos+3]=float(str.split(f[ligne])[3].replace("D","E"))
				S[pos+3][pos+2]=S[pos+2][pos+3]
				S[pos+3][pos+3]=float(str.split(f[ligne])[4].replace("D","E"))
	return([S,t3])

def coef(ngroup,reste,nbasis):
	CC=np.zeros((nbasis,nbasis))
	f=os.popen("grep -A"+str((ngroup+1)*(nbasis+3))+" 'Molecular Orbital Coefficients' "+fichier).readlines()
	ligne=1
	orb=[]
	orb2=[]
	for i in range(0,ngroup):
		ligne=ligne+3
		for j in range(0,nbasis):
			CC[i*5][j]=float(str.split(f[ligne])[-5])
			CC[i*5+1][j]=float(str.split(f[ligne])[-4])
			CC[i*5+2][j]=float(str.split(f[ligne])[-3])
			CC[i*5+3][j]=float(str.split(f[ligne])[-2])
			CC[i*5+4][j]=float(str.split(f[ligne])[-1])
			if i==0:
				if str.split(f[ligne])[1].isdigit():
					#print str.split(f[ligne])
					orb2.append([str.split(f[ligne])[1],str.split(f[ligne])[2]])
					t1=str.split(f[ligne])[1]
					t2=str.split(f[ligne])[2]
					orb.append([t1,t2,str.split(f[ligne])[3]])
				else:
					if str.split(f[ligne])[2]=="0":
						orb.append([t1,t2,str.split(f[ligne])[1]+str.split(f[ligne])[2]])
					else:
						orb.append([t1,t2,str.split(f[ligne])[1]])
					
			ligne=ligne+1
	ligne=ligne+3
	if reste>0:
		for j in range(0,nbasis):
			for i in range(0,reste):
				CC[ngroup*5+i][j]=float(str.split(f[ligne])[-1*reste+i])
			ligne=ligne+1
	return(CC,orb,orb2)


def coef_alpha(ngroup,reste,nbasis):
	CC=np.zeros((nbasis,nbasis))
	f=os.popen("grep -A"+str((ngroup+1)*(nbasis+3))+" 'Alpha Molecular Orbital Coefficients' "+fichier).readlines()
	ligne=1
	orb=[]
	orb2=[]
	for i in range(0,ngroup):
		ligne=ligne+3
		for j in range(0,nbasis):
			CC[i*5][j]=float(str.split(f[ligne])[-5])
			CC[i*5+1][j]=float(str.split(f[ligne])[-4])
			CC[i*5+2][j]=float(str.split(f[ligne])[-3])
			CC[i*5+3][j]=float(str.split(f[ligne])[-2])
			CC[i*5+4][j]=float(str.split(f[ligne])[-1])
			if i==0:
				if str.split(f[ligne])[1].isdigit():
					#print str.split(f[ligne])
					orb2.append([str.split(f[ligne])[1],str.split(f[ligne])[2]])
					t1=str.split(f[ligne])[1]
					t2=str.split(f[ligne])[2]
					orb.append([t1,t2,str.split(f[ligne])[3]])
				else:
					if str.split(f[ligne])[2]=="0":
						orb.append([t1,t2,str.split(f[ligne])[1]+str.split(f[ligne])[2]])
					else:
						orb.append([t1,t2,str.split(f[ligne])[1]])
					
			ligne=ligne+1
	ligne=ligne+3
	if reste>0:
		for j in range(0,nbasis):
			for i in range(0,reste):
				CC[ngroup*5+i][j]=float(str.split(f[ligne])[-1*reste+i])
			ligne=ligne+1
	return(CC,orb,orb2)





def coef_beta(ngroup,reste,nbasis):
	CC=np.zeros((nbasis,nbasis))
	f=os.popen("grep -A"+str((ngroup+1)*(nbasis+3))+" 'Beta Molecular Orbital Coefficients' "+fichier).readlines()
	ligne=1
	orb=[]
	orb2=[]
	for i in range(0,ngroup):
		ligne=ligne+3
		for j in range(0,nbasis):
			CC[i*5][j]=float(str.split(f[ligne])[-5])
			CC[i*5+1][j]=float(str.split(f[ligne])[-4])
			CC[i*5+2][j]=float(str.split(f[ligne])[-3])
			CC[i*5+3][j]=float(str.split(f[ligne])[-2])
			CC[i*5+4][j]=float(str.split(f[ligne])[-1])
			if i==0:
				if str.split(f[ligne])[1].isdigit():
					#print str.split(f[ligne])
					orb2.append([str.split(f[ligne])[1],str.split(f[ligne])[2]])
					t1=str.split(f[ligne])[1]
					t2=str.split(f[ligne])[2]
					orb.append([t1,t2,str.split(f[ligne])[3]])
				else:
					if str.split(f[ligne])[2]=="0":
						orb.append([t1,t2,str.split(f[ligne])[1]+str.split(f[ligne])[2]])
					else:
						orb.append([t1,t2,str.split(f[ligne])[1]])
					
			ligne=ligne+1
	ligne=ligne+3
	if reste>0:
		for j in range(0,nbasis):
			for i in range(0,reste):
				CC[ngroup*5+i][j]=float(str.split(f[ligne])[-1*reste+i])
			ligne=ligne+1
	return(CC,orb,orb2)


def ho():
	f=os.popen("grep 'Alpha  occ. eigenvalues --' "+fichier).readlines()
	n=(len(f)-1)*5+len(str.split(f[len(f)-1]))-4
	return(n)

def purcent_scpa(overlap,coeff,nb):
	p=np.zeros(nb)
	som=0.0
	for j in range(0,nb):
		som=som+coeff[j]**2
	for i in range(0,nb):
		p[i]=100.0*coeff[i]**2/som
	return(p)

def purcent_modmul(overlap,coeff,nb):
	p=np.zeros(nb)
	for i in range(0,nb):
		p[i]=coeff[i]**2
		for j in range(0,nb):
			if j!=i:
				if coeff[i]**2+coeff[j]**2!=0:
					p[i]=p[i]+2.0*coeff[i]*coeff[j]*overlap[i][j]*coeff[i]**2/(coeff[i]**2+coeff[j]**2)*100.0
				#else:
				#	print(coeff[i],coeff[j])
	return(p)

def purcent_mul(overlap,coeff,nb):
	p=np.zeros(nb)
	for i in range(0,nb):
		for j in range(0,nb):
			p[i]=p[i]+coeff[i]*coeff[j]*overlap[i][j]*100.0
	return(p)
print('ici')
S1=overlap(ngroup,reste,nbasis)
print('ici')
S=S1[0]
f=os.popen("grep 'Beta  Orbitals:' "+fichier+" | wc -l").readlines()
if int(str.split(f[0])[0])==0:
	res=coef(ngroup,reste,nbasis)
else:
	print("YOUR CALCULATION IS UNRESTRICTED")
	print("DO YOU WANT ALPHA (CHOICE 1) or BETA (CHOICE2) ORBITALS")
	spin=int(raw_input(""))
	if spin==1:
		res=coef_alpha(ngroup,reste,nbasis)
	elif spin==2:
		res=coef_beta(ngroup,reste,nbasis)
C=res[0]
orbital=res[1]
atoms=res[2]
HOMO=ho()
print("THE HOMO ORBITAL IS ORBITAL "+str(HOMO))

orb_list=[]
print ("ORBITAL LIST (Ex: 1 2-5 12)")
orb2=raw_input("")

#print(orbital)
if S1[1]==1:
	print("WHICH KIND OF DECOMPOSITION")
	print("1: MULLIKEN")
	print("2: MODIFIED MULLIKEN")
	print("3: ROS ET SCHUIT")
	met=int(raw_input(""))
else:
	met=1
print("DO YOU WANT DECOMPOSITION OVER ATOMS (CHOICE 1) OR ORBITALS (CHOICE 2)")
decomp=int(raw_input(""))
orb_list=[]
#print ("ORBITAL LIST (Ex: 1 2-5 12)")
#orb2=raw_input("")
#print str.split(orb2)
for i in range(0,len(str.split(orb2))):
	if not "-" in str.split(orb2)[i]:
		orb_list.append(int(str.split(orb2)[i])-1)
	else:
		pos=0
		for j in range(0,len(str.split(orb2)[i])):
			if str.split(orb2)[i][j]=="-":
				pos=j
				break
		#print str.split(orb2)[i][:pos],str.split(orb2)[i][pos+1:]
		for j in range(int(str.split(orb2)[i][:pos])-1,int(str.split(orb2)[i][pos+1:])):
			orb_list.append(j)
#print orb_list
for i in range(0,len(orb_list)):
	orb=orb_list[i]
	print(orb)
	if met==1:
		purcent=purcent_mul(S,C[orb],nbasis)
	#	print(purcent)
	#	som=0
	#	for i in range(0,len(purcent)):
	#		som=som+purcent[i]
	#		if purcent[i]>1:print(purcent[i])
	#	print(som)
	if met==2:
		purcent=purcent_modmul(S,C[orb],nbasis)
	
	if met==3:
		purcent=purcent_scpa(S,C[orb],nbasis)
	
	if decomp==1:
		atlist=[]
		at=[]
		ecrire=[[" atome "," symbole "," purcent "]]
		for i in range(0,nat):
			at.append([])
		for i in range(0,len(orbital)):
			at[int(orbital[i][0])-1].append(i)
			if int(orbital[i][0])!=len(atlist):atlist.append(orbital[i][1])
		#print(at,atlist)
		for i in range(0,len(at)):
			som=0
			for j in range(0,len(at[i])):
				som=som+purcent[at[i][j]]
			ecrire.append([str(i+1),atlist[i],str("%.2f" % som)])
		ecrire2=[]
		ecrire2.append(ecrire[0][0]+"|"+ecrire[0][1]+"|"+ecrire[0][2]+"|")
		for i in range(1,len(ecrire)):
			ecrire3=""
			for j in range(0,len(ecrire[i])):
				ecrire3=ecrire3+ecrire[i][j]
				for k in range(len(ecrire[i][j]),len(ecrire[0][j])):
					ecrire3=ecrire3+" "
				ecrire3=ecrire3+"|"
			ecrire2.append(ecrire3)
		ecriture=open(fichier+"_orb_"+str(orb+1)+"_per-at",'wb')
		for i in range(0,len(ecrire2)):
			ecriture.write(ecrire2[i]+"\n")
		ecriture.close()
	if decomp==2:
		ecrire=[[" atome "," symbole "," orbital "," purcent "]]
		ecrire2=[]
		ecrire2.append(ecrire[0][0]+"|"+ecrire[0][1]+"|"+ecrire[0][2]+"|"+ecrire[0][3]+"|")
		for i in range(0,len(purcent)):
			ecrire.append([str(orbital[i][0]),orbital[i][1],orbital[i][2],str("%.2f" %purcent[i])])
		for i in range(1,len(ecrire)):
			ecrire3=""
			for j in range(0,len(ecrire[i])):
				ecrire3=ecrire3+ecrire[i][j]
				for k in range(len(ecrire[i][j]),len(ecrire[0][j])):
					ecrire3=ecrire3+" "
				ecrire3=ecrire3+"|"
			ecrire2.append(ecrire3)
		ecriture=open(fichier+"_orb_"+str(orb+1)+"_per-orb",'wb')
		for i in range(0,len(ecrire2)):
			ecriture.write(ecrire2[i]+"\n")
		ecriture.close()		
	#	orbitals2=[]
	#	for i in range(0,len(
	
